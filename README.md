# This is my stab at creating a wrapper for the League of Legends API. 
# All functionality is supported and I will update this whenever Riot updates the API
# Feel free to use this in any project you are working on. Just create a JLoLPI object and you will have access to all the functions needed to gather league data.
# A downloadable jar file can be found in the downloads section.
Updated to version 1.1 to support v2.3 of League data.