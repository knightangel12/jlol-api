package gamemodel;

import java.util.List;

public class Team {
	long createDate;
	String fullId;
	long lastGameDate;
	long lastJoinDate;
	long lastJoinedRankedTeamQueueDate;
	List<MatchHistorySummary> matchHistory;
	MessageOfDay messageOfDay;
	long ModifyDate;
	String name;
	Roster roster;
	long SecondLastJoinDate;
	String status;
	String tag;
	TeamStatSummary teamStatSummary;
	long thirdLastJoinDate;
	public long getCreateDate() {
		return createDate;
	}
	public void setCreateDate(long createDate) {
		this.createDate = createDate;
	}
	public String getFullId() {
		return fullId;
	}
	public void setFullId(String fullId) {
		this.fullId = fullId;
	}
	public long getLastGameDate() {
		return lastGameDate;
	}
	public void setLastGameDate(long lastGameDate) {
		this.lastGameDate = lastGameDate;
	}
	public long getLastJoinDate() {
		return lastJoinDate;
	}
	public void setLastJoinDate(long lastJoinDate) {
		this.lastJoinDate = lastJoinDate;
	}
	public long getLastJoinedRankedTeamQueueDate() {
		return lastJoinedRankedTeamQueueDate;
	}
	public void setLastJoinedRankedTeamQueueDate(long lastJoinedRankedTeamQueueDate) {
		this.lastJoinedRankedTeamQueueDate = lastJoinedRankedTeamQueueDate;
	}
	public List<MatchHistorySummary> getMatchHistory() {
		return matchHistory;
	}
	public void setMatchHistory(List<MatchHistorySummary> matchHistory) {
		this.matchHistory = matchHistory;
	}
	public MessageOfDay getMessageOfDay() {
		return messageOfDay;
	}
	public void setMessageOfDay(MessageOfDay messageOfDay) {
		this.messageOfDay = messageOfDay;
	}
	public long getModifyDate() {
		return ModifyDate;
	}
	public void setModifyDate(long modifyDate) {
		ModifyDate = modifyDate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Roster getRoster() {
		return roster;
	}
	public void setRoster(Roster roster) {
		this.roster = roster;
	}
	public long getSecondLastJoinDate() {
		return SecondLastJoinDate;
	}
	public void setSecondLastJoinDate(long secondLastJoinDate) {
		SecondLastJoinDate = secondLastJoinDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public TeamStatSummary getTeamStatSummary() {
		return teamStatSummary;
	}
	public void setTeamStatSummary(TeamStatSummary teamStatSummary) {
		this.teamStatSummary = teamStatSummary;
	}
	public long getThirdLastJoinDate() {
		return thirdLastJoinDate;
	}
	public void setThirdLastJoinDate(long thirdLastJoinDate) {
		this.thirdLastJoinDate = thirdLastJoinDate;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tcreateDate: " + createDate
				+ "\n\tfullId: " + fullId + "\n\tlastGameDate: " + lastGameDate
				+ "\n\tlastJoinDate: " + lastJoinDate
				+ "\n\tlastJoinedRankedTeamQueueDate: "
				+ lastJoinedRankedTeamQueueDate + "\n\tmatchHistory: "
				+ matchHistory + "\n\tmessageOfDay: " + messageOfDay
				+ "\n\tModifyDate: " + ModifyDate + "\n\tname: " + name
				+ "\n\troster: " + roster + "\n\tSecondLastJoinDate: "
				+ SecondLastJoinDate + "\n\tstatus: " + status + "\n\ttag: "
				+ tag + "\n\tteamStatSummary: " + teamStatSummary
				+ "\n\tthirdLastJoinDate: " + thirdLastJoinDate + "\n}";
	}
	

}
