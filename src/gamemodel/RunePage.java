package gamemodel;

import java.util.List;

public class RunePage {
	boolean current;
	long id;
	String name;
	List<RuneSlot> slots;
	public boolean isCurrent() {
		return current;
	}
	public void setCurrent(boolean current) {
		this.current = current;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<RuneSlot> getSlots() {
		return slots;
	}
	public void setSlots(List<RuneSlot> slots) {
		this.slots = slots;
	}
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tcurrent: " + current + "\n\tid: "
				+ id + "\n\tname: " + name + "\n\tslots: " + slots + "\n}";
	}
	
	

}
