package gamemodel;

import java.util.Set;

public class TeamStatSummary {
	String fullId;
	Set<TeamStatDetail> teamStatDetails;
	
	public String getFullId() {
		return fullId;
	}
	public void setFullId(String fullId) {
		this.fullId = fullId;
	}
	public Set<TeamStatDetail> getTeamStatDetails() {
		return teamStatDetails;
	}
	public void setTeamStatDetails(Set<TeamStatDetail> teamStatDetails) {
		this.teamStatDetails = teamStatDetails;
	}
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tfullId: " + fullId
				+ "\n\tteamStatDetails: " + teamStatDetails + "\n}";
	}
	
}
