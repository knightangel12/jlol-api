package gamemodel;

public class TeamStatDetail {
	int averageGamesPlayed;
	String fullId;
	int losses;
	String teamStatType;
	int wins;
	public int getAverageGamesPlayed() {
		return averageGamesPlayed;
	}
	public void setAverageGamesPlayed(int averageGamesPlayed) {
		this.averageGamesPlayed = averageGamesPlayed;
	}
	public String getFullId() {
		return fullId;
	}
	public void setFullId(String fullId) {
		this.fullId = fullId;
	}
	public int getLosses() {
		return losses;
	}
	public void setLosses(int losses) {
		this.losses = losses;
	}
	public String getTeamStatType() {
		return teamStatType;
	}
	public void setTeamStatType(String teamStatType) {
		this.teamStatType = teamStatType;
	}
	public int getWins() {
		return wins;
	}
	public void setWins(int wins) {
		this.wins = wins;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\taverageGamesPlayed: "
				+ averageGamesPlayed + "\n\tfullId: " + fullId + "\n\tlosses: "
				+ losses + "\n\tteamStatType: " + teamStatType + "\n\twins: "
				+ wins + "\n}";
	}
	
	

}
