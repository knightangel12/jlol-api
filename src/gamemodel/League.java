package gamemodel;

import java.util.List;

public class League {
	List<LeagueItem> entries;
	String name;
	String participantId;
	String queue;
	String tier;
	
	public List<LeagueItem> getEntries() {
		return entries;
	}
	public void setEntries(List<LeagueItem> entries) {
		this.entries = entries;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getQueue() {
		return queue;
	}
	public void setQueue(String queue) {
		this.queue = queue;
	}
	public String getTier() {
		return tier;
	}
	public void setTier(String tier) {
		this.tier = tier;
	}
	public String getParticipantId() {
		return participantId;
	}
	public void setParticipantId(String participantId) {
		this.participantId = participantId;
	}
	
	//TODO: update toString
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tname: " + name + "\n\tqueue: "
				+ queue + "\n\ttier: " + tier + "\n\tentries: " + entries
				+ "\n}";
	}
	
	

}
