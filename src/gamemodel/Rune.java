package gamemodel;

public class Rune {
	String description;
	int id;
	String name;
	int tier;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getTier() {
		return tier;
	}
	public void setTier(int tier) {
		this.tier = tier;
	}
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tdescription: " + description
				+ "\n\tid: " + id + "\n\tname: " + name + "\n\ttier: " + tier
				+ "\n}";
	}
	
	
}
