package gamemodel;

import java.util.Set;

public class MasteryPages {
	Set<MasteryPage> pages;
	long summonerId;
	
	public Set<MasteryPage> getPages() {
		return pages;
	}
	public void setPages(Set<MasteryPage> pages) {
		this.pages = pages;
	}
	public long getSummonerId() {
		return summonerId;
	}
	public void setSummonerId(long summonerId) {
		this.summonerId = summonerId;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tsummonerId: " + summonerId
				+ "\n\tpages: " + pages + "\n}";
	}
	
	

}
