package gamemodel;


public class MiniSeries {
	private int losses;
	private String progress;
	private int target;
	private long timeLeftToPlayMillis;
	private int wins;
	
	public int getLosses() {
		return losses;
	}
	public void setLosses(int losses) {
		this.losses = losses;
	}
	public String getProgress() {
		return progress;
	}
	public void setProgress(String progress) {
		this.progress = progress;
	}
	public int getTarget() {
		return target;
	}
	public void setTarget(int target) {
		this.target = target;
	}
	public long getTimeLeftToPlay() {
		return timeLeftToPlayMillis;
	}
	public void setTimeLeftToPlay(long timeLeftToPlayMillis) {
		this.timeLeftToPlayMillis = timeLeftToPlayMillis;
	}
	public int getWins() {
		return wins;
	}
	public void setWins(int wins) {
		this.wins = wins;
	}
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tlosses: " + losses
				+ "\n\tprogress: " + progress + "\n\ttarget: " + target
				+ "\n\ttimeLeftToPlay: " + timeLeftToPlayMillis + "\n\twins: " + wins
				+ "\n}";
	}
	
	
}
