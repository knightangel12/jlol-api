package gamemodel;

public class Champion 
{
	private boolean active;
	private int attackRank;
	private boolean botEnabled;
	private boolean botMmEnabled;
	private int defenceRank;
	private int difficultyRank;
	private boolean freeToPlay;
	private long id;
	private int magicRank;
	private String name;
	private boolean rankedPlayEnabled;
	
	
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public int getAttackRank() {
		return attackRank;
	}
	public void setAttackRank(int attackRank) {
		this.attackRank = attackRank;
	}
	public boolean isBotEnabled() {
		return botEnabled;
	}
	public void setBotEnabled(boolean botEnabled) {
		this.botEnabled = botEnabled;
	}
	public boolean isBotMmEnabled() {
		return botMmEnabled;
	}
	public void setBotMmEnabled(boolean botMmEnabled) {
		this.botMmEnabled = botMmEnabled;
	}
	public int getDefenceRank() {
		return defenceRank;
	}
	public void setDefenceRank(int defenceRank) {
		this.defenceRank = defenceRank;
	}
	public int getDifficultyRank() {
		return difficultyRank;
	}
	public void setDifficultyRank(int difficultyRank) {
		this.difficultyRank = difficultyRank;
	}
	public boolean isFreeToPlay() {
		return freeToPlay;
	}
	public void setFreeToPlay(boolean freeToPlay) {
		this.freeToPlay = freeToPlay;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getMagicRank() {
		return magicRank;
	}
	public void setMagicRank(int magicRank) {
		this.magicRank = magicRank;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isRankedPlayEnabled() {
		return rankedPlayEnabled;
	}
	public void setRankedPlayEnabled(boolean rankedPlayEnabled) {
		this.rankedPlayEnabled = rankedPlayEnabled;
	}
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tactive: " + active
				+ "\n\tattackRank: " + attackRank + "\n\tbotEnabled: "
				+ botEnabled + "\n\tbotMmEnabled: " + botMmEnabled
				+ "\n\tdefenceRank: " + defenceRank + "\n\tdifficultyRank: "
				+ difficultyRank + "\n\tfreeToPlay: " + freeToPlay + "\n\tid: "
				+ id + "\n\tmagicRank: " + magicRank + "\n\tname: " + name
				+ "\n\trankedPlayEnabled: " + rankedPlayEnabled + "\n}";
	}
	
	
}
