package gamemodel;

import java.util.List;

public class RankedStats {
	List<ChampionStats> champions;
	long modifyDate;
	long summonerId;
	
	public List<ChampionStats> getChampions() {
		return champions;
	}
	public void setChampions(List<ChampionStats> champions) {
		this.champions = champions;
	}
	public long getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(long modifyDate) {
		this.modifyDate = modifyDate;
	}
	public long getSummonerId() {
		return summonerId;
	}
	public void setSummonerId(long summonerId) {
		this.summonerId = summonerId;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tmodifyDate: " + modifyDate
				+ "\n\tsummonerId: " + summonerId + "\n\tchampions: "
				+ champions + "\n}";
	}
	
	
}
