package gamemodel;

public class MatchHistorySummary {
	int assists;
	int deaths;
	long gameId;
	String gameMode;
	boolean invalid;
	int kills;
	int mapId;
	int opposingTeamKills;
	String opposingTeamName;
	boolean win;
	
	public int getAssists() {
		return assists;
	}
	public void setAssists(int assists) {
		this.assists = assists;
	}
	public int getDeaths() {
		return deaths;
	}
	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}
	public long getGameId() {
		return gameId;
	}
	public void setGameId(long gameId) {
		this.gameId = gameId;
	}
	public String getGameMode() {
		return gameMode;
	}
	public void setGameMode(String gameMode) {
		this.gameMode = gameMode;
	}
	public boolean isInvalid() {
		return invalid;
	}
	public void setInvalid(boolean invalid) {
		this.invalid = invalid;
	}
	public int getKills() {
		return kills;
	}
	public void setKills(int kills) {
		this.kills = kills;
	}
	public int getMapId() {
		return mapId;
	}
	public void setMapId(int mapId) {
		this.mapId = mapId;
	}
	public int getOpposingTeamKills() {
		return opposingTeamKills;
	}
	public void setOpposingTeamKills(int opposingTeamKills) {
		this.opposingTeamKills = opposingTeamKills;
	}
	public String getOpposingTeamName() {
		return opposingTeamName;
	}
	public void setOpposingTeamName(String opposingTeamName) {
		this.opposingTeamName = opposingTeamName;
	}
	public boolean isWin() {
		return win;
	}
	public void setWin(boolean win) {
		this.win = win;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tassists: " + assists
				+ "\n\tdeaths: " + deaths + "\n\tgameId: " + gameId
				+ "\n\tgameMode: " + gameMode + "\n\tinvalid: " + invalid
				+ "\n\tkills: " + kills + "\n\tmapId: " + mapId
				+ "\n\topposingTeamKills: " + opposingTeamKills
				+ "\n\topposingTeamName: " + opposingTeamName + "\n\twin: "
				+ win + "\n}";
	}
	
	
}
