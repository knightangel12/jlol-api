package gamemodel;

import java.util.Set;

public class RecentGames {
	Set<Game> games;
	long summonerId;
	
	public Set<Game> getGames() {
		return games;
	}
	public void setGames(Set<Game> games) {
		this.games = games;
	}
	public Long getSummonerId() {
		return summonerId;
	}
	public void setSummonerId(Long summonerId) {
		this.summonerId = summonerId;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tsummonerId: " + summonerId
				+ "\n\tgames: " + games + "\n}";
	}
	
	
}
