package gamemodel;

public class PlayerStatsSummary {
	AggregatedStats aggregatedStats;
	int losses;
	long modifyDate;
	String playerStatSummaryType;
	int wins;
	
	public AggregatedStats getAggregatedStats() {
		return aggregatedStats;
	}
	public void setAggregatedStats(AggregatedStats aggregatedStats) {
		this.aggregatedStats = aggregatedStats;
	}
	public int getLosses() {
		return losses;
	}
	public void setLosses(int losses) {
		this.losses = losses;
	}
	public long getModifyDate() {
		return modifyDate;
	}
	public void setModifyDate(long modifyDate) {
		this.modifyDate = modifyDate;
	}
	public String getPlayerStatSummaryType() {
		return playerStatSummaryType;
	}
	public void setPlayerStatSummaryType(String playerStatSummaryType) {
		this.playerStatSummaryType = playerStatSummaryType;
	}
	public int getWins() {
		return wins;
	}
	public void setWins(int wins) {
		this.wins = wins;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tlosses: " + losses
				+ "\n\tmodifyDate: " + modifyDate
				+ "\n\tplayerStatSummaryType: " + playerStatSummaryType
				+ "\n\twins: " + wins + "\n\taggregatedStats: "
				+ aggregatedStats + "\n}";
	}
	
	
	
	

}
