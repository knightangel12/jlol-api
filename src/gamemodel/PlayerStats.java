package gamemodel;

import java.util.List;

public class PlayerStats {
	List<PlayerStatsSummary> playerStatSummaries;
	long summonerId;
	
	
	
	public List<PlayerStatsSummary> getPlayerStatSummaries() {
		return playerStatSummaries;
	}



	public void setPlayerStatSummaries(List<PlayerStatsSummary> playerStatSummaries) {
		this.playerStatSummaries = playerStatSummaries;
	}



	public long getSummonerId() {
		return summonerId;
	}



	public void setSummonerId(long summonerId) {
		this.summonerId = summonerId;
	}



	@Override
	public String toString() {
		return getClass().getName() + " {\n\tsummonerId: " + summonerId
				+ "\n\tplayerStatSummaries: " + playerStatSummaries + "\n}";
	}

}
