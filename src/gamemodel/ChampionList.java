package gamemodel;

import java.util.ArrayList;

public class ChampionList {
	private ArrayList<Champion> champions;

	public ArrayList<Champion> getChampionList() {
		return champions;
	}

	public void setChampionList(ArrayList<Champion> championList) {
		this.champions = championList;
	}

	@Override
	public String toString() {
		return getClass().getName() + " {\n\tchampions: " + champions + "\n}";
	}
	
	
}
