package gamemodel;


public class TeamMemberInfo {
	long inviteDate;
	long joinDate;
	long playerId;
	String status;
	public long getInviteDate() {
		return inviteDate;
	}
	public void setInviteDate(long inviteDate) {
		this.inviteDate = inviteDate;
	}
	public long getJoinDate() {
		return joinDate;
	}
	public void setJoinDate(long joinDate) {
		this.joinDate = joinDate;
	}
	public long getPlayerId() {
		return playerId;
	}
	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tinviteDate: " + inviteDate
				+ "\n\tjoinDate: " + joinDate + "\n\tplayerId: " + playerId
				+ "\n\tstatus: " + status + "\n}";
	}
	
	
}
