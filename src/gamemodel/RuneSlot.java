package gamemodel;

public class RuneSlot {
	Rune rune;
	int runeSlotId;
	public Rune getRune() {
		return rune;
	}
	public void setRune(Rune rune) {
		this.rune = rune;
	}
	public int getRuneSlotId() {
		return runeSlotId;
	}
	public void setRuneSlotId(int runeSlotId) {
		this.runeSlotId = runeSlotId;
	}
	@Override
	public String toString() {
		return getClass().getName() + " {\n\truneSlotId: " + runeSlotId
				+ "\n\trune: " + rune + "\n}";
	}

}
