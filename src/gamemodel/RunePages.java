package gamemodel;

import java.util.Set;

public class RunePages {
	Set<RunePage> pages;
	long summonerId;
	
	public Set<RunePage> getPages() {
		return pages;
	}
	public void setPages(Set<RunePage> pages) {
		this.pages = pages;
	}
	public long getSummonerId() {
		return summonerId;
	}
	public void setSummonerId(long summonerId) {
		this.summonerId = summonerId;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tpages: " + pages
				+ "\n\tsummonerId: " + summonerId + "\n}";
	}
	
	

}
