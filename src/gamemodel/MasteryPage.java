package gamemodel;

import java.util.List;

public class MasteryPage {
	boolean current;
	long id;
	String name;
	List<Talent> talents;
	
	public boolean isCurrent() {
		return current;
	}
	public void setCurrent(boolean current) {
		this.current = current;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<Talent> getTalents() {
		return talents;
	}
	public void setTalents(List<Talent> talents) {
		this.talents = talents;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tcurrent: " + current + "\n\tid: "
				+ id + "\n\tname: " + name + "\n\ttalents: " + talents + "\n}";
	}

	
	
}
