package gamemodel;

public class ChampionStats {
	int id;
	String name;
	AggregatedStats stats;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public AggregatedStats getStats() {
		return stats;
	}
	public void setStats(AggregatedStats stats) {
		this.stats = stats;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tid: " + id + "\n\tname: " + name
				+ "\n\tstats: " + stats + "\n}";
	}
	
	

}
