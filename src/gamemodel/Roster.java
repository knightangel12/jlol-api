package gamemodel;

import java.util.List;

public class Roster {
	List<TeamMemberInfo> memberList;
	long ownerId;
	
	public List<TeamMemberInfo> getMemberList() {
		return memberList;
	}
	public void setMemberList(List<TeamMemberInfo> memberList) {
		this.memberList = memberList;
	}
	public long getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(long ownerId) {
		this.ownerId = ownerId;
	}
	
	@Override
	public String toString() {
		return getClass().getName() + " {\n\tmemberList: " + memberList
				+ "\n\townerId: " + ownerId + "\n}";
	}
	
	
}
