package apicontrol;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import gamemodel.ChampionList;
import gamemodel.League;
import gamemodel.LeagueItem;
import gamemodel.MasteryPages;
import gamemodel.PlayerStats;
import gamemodel.RankedStats;
import gamemodel.RecentGames;
import gamemodel.RunePages;
import gamemodel.Summoner;
import gamemodel.Team;

public class JLoLPI {
	String apiKey="?api_key=";
	String region;
	APICaller apic;
	Gson gson;
	String data;
	
	private final String CHAMPLISTURL = "/v1.1/champion";
	private final String RECENTGAMESURL1 = "/v1.3/game/by-summoner/";
	private final String RECENTGAMESURL2 = "/recent";
	private final String LEAGUEURL = "/v2.3/league/";
	private final String CHALLENGERLEAGUESURL = "challenger";
	private final String LEAGUEENTRYURL1 = "by-summoner/";
	private final String LEAGUEENTRYURL2 = "/entry";
	private final String LEAGUEDATAURL = "by-summoner/";
	private final String STATSURL1 = "/v1.2/stats/by-summoner/";
	private final String STATSURL2 = "/summary";
	private final String RANKEDSTATSURL = "/ranked";
	private final String SUMMONERURL = "/v1.3/summoner/";
	private final String MASTERIESURL = "/masteries";
	private final String RUNESURL = "/runes";
	private final String TEAMURL = "/v2.2/team/by-summoner/";

	public JLoLPI(String apiKey, String region){
		this.apiKey+=apiKey;
		this.region=region;
		apic = APICaller.getInstance();
		gson = new Gson();
	}
	
	public ChampionList getChampions(){
		data = apic.getData(region + CHAMPLISTURL + apiKey);
		return gson.fromJson(data, ChampionList.class);
	}
	
	public ChampionList getFreeChampions(){
		data = apic.getData(region + CHAMPLISTURL + apiKey + "&freeToPlay=true");
		return gson.fromJson(data, ChampionList.class);
	}
	
	public RecentGames getRecentGames(String summonerId){
		data = apic.getData(region + RECENTGAMESURL1 + summonerId + RECENTGAMESURL2 + apiKey);
		return gson.fromJson(data, RecentGames.class);
	}
	
	public League getChallengerLeague(String queueType){
		//queueType should be RANKED_SOLO_5x5, RANKED_TEAM_5x5, or RANKED_TEAM_3x3.
		data = apic.getData(region + LEAGUEURL + CHALLENGERLEAGUESURL + apiKey + "&type=" + queueType );
		return gson.fromJson(data, League.class);
	}
	
	public List<LeagueItem> getLeagueEntryData(String summonerId){
		data = apic.getData(region + LEAGUEURL + LEAGUEENTRYURL1 + summonerId + LEAGUEENTRYURL2 + apiKey);
		return gson.fromJson(data, new TypeToken<List<LeagueItem>>(){}.getType());
	}
	
	public List<League> getLeagueData(String summonerId){
		data = apic.getData(region + LEAGUEURL + LEAGUEDATAURL + summonerId + apiKey);
		return gson.fromJson(data, new TypeToken<List<League>>(){}.getType());
	}
	
	public PlayerStats getStats(String summonerId, int season ){
		data = apic.getData(region + STATSURL1 + summonerId + STATSURL2 + apiKey + "&season=SEASON"+season);
		return gson.fromJson(data, PlayerStats.class);
	}
	
	public RankedStats getRankedStats(String summonerId, int season ){
		data = apic.getData(region + STATSURL1 + summonerId + RANKEDSTATSURL + apiKey + "&season=SEASON"+season);
		return gson.fromJson(data, RankedStats.class);
	}
	
	public Map<String, MasteryPages> getMasteryPages(String summonerId){
		data = apic.getData(region + SUMMONERURL + summonerId + MASTERIESURL + apiKey);
		return gson.fromJson(data, new TypeToken<Map<String, MasteryPages>>(){}.getType());
	}
	
	public Map<String, RunePages> getRunePages(String summonerId){
		data = apic.getData(region + SUMMONERURL + summonerId + RUNESURL + apiKey);
		return gson.fromJson(data, new TypeToken<Map<String, RunePages>>(){}.getType());
	}
	
	public Map<String, Summoner> getSummonerByName(String summonerName){
		data = apic.getData(region + SUMMONERURL + "by-name/" + summonerName + apiKey);
		return gson.fromJson(data, new TypeToken<Map<String, Summoner>>(){}.getType());
	}
	
	public Map<String, String> getSummonerName(String summonerId){
		data = apic.getData(region + SUMMONERURL + summonerId + "/name" + apiKey);
		return gson.fromJson(data, new TypeToken<Map<String, String>>(){}.getType());
	}
	
	public Map<String, Summoner> getSummonerById(String summonerId){
		data = apic.getData(region + SUMMONERURL + summonerId + apiKey);
		return gson.fromJson(data, new TypeToken<Map<String, Summoner>>(){}.getType());
	}
	
	public ArrayList<Team> getTeam(String summonerId){
		data = apic.getData(region + TEAMURL + summonerId + apiKey);
		return gson.fromJson(data, new TypeToken<ArrayList<Team>>(){}.getType());
	}

}
