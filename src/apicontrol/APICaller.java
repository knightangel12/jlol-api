package apicontrol;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;


public class APICaller {
	private static APICaller instance;
	private final static String URL = "https://prod.api.pvp.net/api/lol/";
	
	private APICaller(){		//default constructor sets region to NA

	}
	
	public static APICaller getInstance(){
		if(instance == null){
			instance = new APICaller();
		}
		
		return instance;
	}
	
	
	protected String getData(String subURL){
		StringBuilder data = new StringBuilder();
		String line="";
		
		try{
			URL url = new URL(URL + subURL);
			HttpURLConnection con = (HttpURLConnection)url.openConnection();
			con.setRequestMethod("GET");
			if(con.getResponseCode()==200){
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
				
				while ((line =br.readLine()) != null){
					data.append(line);
				}	
				
				return data.toString();
				
			}
			else{
				//TODO: figure out a better way to handle GET errors
				return "error-" + con.getResponseCode();
			}

			
		} catch(MalformedURLException e){
			System.out.println("MalExc");
			e.printStackTrace();
		} catch(IOException e){
			System.out.println("IOExc");
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	
	
	
}
